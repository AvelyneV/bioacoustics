options(error = function() traceback(3))

globals <- c('MONO_LEFT')
ok_globals <- TRUE
for (g in globals) {
  if (!exists(g)) {
    print("======= ALERT =======")
    print(sprintf("The variable '%s' is not defined.", g))
    print("This is not gonna work.")
    print("=====================")
    ok_globals <- FALSE
  }
}


library(seewave)
library(tuneR)
library(soundchunk)
library(glue)
source("/home/bjt755/OneDrive/27.Bioacoustics/git-bioacoustics/vocalisation-structure/functions/get_formants.R")

## set up the player if a path is specified
if(path_to_player != ''){
  tuneR::setWavPlayer(path_to_player)
}

## FUNCTION SPECTRO_TEMPORAL_FEATURES_DIR
baaacoustics_dir <- function(path_to_data,
                             min_freq, max_freq, min_dur,
                             min_f0, max_f0, wl_fft_analysis,
                             ov_fft, wl_env, ov_env,
                             amp_th, security, 
                             formants_bands = NULL,
                             wl_fft_formants = NULL){
  ## --------------------------
  
  ## Arguments:
  
  ## path_to_data = string, path to directory where stored wav file(s)
  
  ## path_to_analyses = string, path to directory where the
  ## spectro_temporal_features file will be stored
  
  ## min_freq, max_freq = numerical minimum and maximum frequencies of
  ## interest in Hz (file is filter according to)
  
  ## min_dur = numerical, minimum duration for vocalization detection
  ## in file (in seconds)
  
  ## min_f0, max_f0 = numerical, frequency bandwidth between which the
  ## fundamental frequency sould be searched for in Hz
  
  ## wl_fft_analysis = numerical, length of the window for FFT computation
  ## (should be a multiple of 256).
  
  ## ov_fft = percentage of overlap between two fft windows
  
  ## wl_env = numerical, length of the window for envelop computation
  ## in points
  
  ## ov_env = percentage of overlap between two env windows
  
  ## amp_th = percentage of amplitude to look for peaks in env
  ## or spectrum
  
  ## formants_bands =  a vector of minimum length 1 containing the boundaries of 
  ## expected formants. Eg. formants_bands <- c(1500, 2800, 4500). Default is NULL.
  
  ## --------------------------
  
  ## ACTION
  
  ## Will run spectro_temporal_features_voc() for each file of the
  ## directory with WAV_EXTENSION
  
  ## --------------------------
  
  ## OUTPUT
  
  ## return a list of two elements
  
  ## - a dataframe with all spectro temporal features of the
  ## vocalization, along with the arguments used to detect them
  
  ## - a dataframe of two columns: file and error type in case the
  ## file could not be analysed.
  
  ## save a spectro_temporal_features_dir.tsv and a
  ## spectro_temporal_features_errors.tsv files in path_to_analyses
  ## (tab separated table).
  
  ## --------------------------
  
  ## keep info show steps or not
  show_steps_tmp <- show_steps()
  
  ## initialize res_dir
  res_dir <- data.frame()
  
  ## initialize error_list
  error_list <- data.frame()
  
  ## get all files
  files <- list.files(path_to_data, get_wav_ext())
  
  tryCatch(
    {
      ## loop for each file
      ## initialize file_index
      index_file <- 1
      for(file in files){
        if(index_file == 1 && show_steps()){
          catn("Showing steps on first file")
          pause()
        }
        
        spectro_temp_feat <- baaacoustics(path_to_data = path_to_data, 
                                          wav_file_name = file, 
                                          min_freq = min_freq, max_freq = max_freq, 
                                          min_dur = min_dur, 
                                          min_f0 = min_f0, max_f0 = max_f0, 
                                          wl_fft_analysis = wl_fft_analysis, 
                                          ov_fft = ov_fft, 
                                          wl_env = wl_env, ov_env = ov_env, 
                                          amp_th = amp_th, 
                                          security = security, 
                                          formants_bands = formants_bands, 
                                          wl_fft_formants = wl_fft_formants)
        
        res_dir <- rbind(res_dir,spectro_temp_feat[[1]])
        error_list <- rbind(error_list,spectro_temp_feat[[2]])
        
        if(index_file == 1 && show_steps()){
          catn("Fist file is done. Mode Show steps mode = FALSE for next files")
          pause()
        }
        set_show_steps(FALSE)
        index_file <- 2
        
      }
    },  error = function(cond){
      catn('ALERT ERROR! Program stopped')
      catn('Remove the file that stopped from the folder and run again')
      message(cond)
      pause()
    } , warning = function(cond){
      message(cond)
      pause()
    }, finally = {
      set_show_steps(show_steps_tmp)
    }
  )
  
  res_dir_file_name <- file.path(path_to_analyses,
                                 'baa_features_data.tsv')
  write.table(res_dir, res_dir_file_name, sep = '\t', row.names = F,
              quote = F)
  errors_file_name <- file.path(path_to_analyses,
                                'baa_features_errors.tsv')
  write.table(error_list, errors_file_name, sep = '\t', row.names = F,
              quote = F)
  
  out <- list(res_dir, error_list)
  names(out) <- c('acoustic_features', 'failed_voc')
  return(out)
}



## FUNCTION SPECTRO_TEMPORAL_FEATURES_VOC
baaacoustics <- function(path_to_data, wav_file_name,
                         min_freq, max_freq, min_dur,
                         min_f0, max_f0, wl_fft_analysis,
                         ov_fft, wl_env, ov_env,
                         amp_th, security, 
                         formants = FALSE,
                         formants_bands = NULL,
                         wl_fft_formants = NULL){
  
  ## --------------------------
  
  ## Arguments:
  
  ## path_to_data = string, path to directory where stored wav file(s)
  
  ## wav_file_name = string, name of wav file (WITH extension)
  
  ## min_freq, max_freq = numerical minimum and maximum frequencies of
  ## interest in Hz (file is be filter according to)
  
  ## min_dur = numerical, minimum duration from vocalization detection
  ## in file in sec
  
  ## min_f0, max_f0 = numerical, frequency bandwidth between which the
  ## fundamental frequency sould be searched for in Hz
  
  ## wl_fft_analysis = numerical, length of the window for FFT computation
  ## (should be a multiple of 256) for spectral analyses
  
  ## ov_fft = percentage of overlap between two fft windows
  
  ## wl_env = numerical, length of the window for envelop computation
  ## in points
  
  ## ov_env = percentage of overlap between two env windows
  
  ## amp_th = percentage of amplitude to look for peaks in env
  ## or spectrum
  
  ## formants = Boolean - whether formants should be looked for. 
  ## Default is FALSE.
  
  ## formants_bands =  either a numerical number with the number of expected formants. 
  ## OR a vector of minimum length 2 containing the boundaries of expected formants. 
  ## Eg. formants_bands <- c(1500, 2800, 4500). Default is NULL.
  ## 
  ## wl_fft_formants = numerical - the length in points of the FFT for formant detection.
  ##  Default is NULL
  
  
  ## --------------------------
  
  ## ACTION
  
  ## Will use ffilter() to filter between min_freq and max_freq,
  ## normalize the sound the sound, use timer() to detect real start
  ## and end using amp_th, compute meanspec() based on wl_fft_analysis
  ## and ov_fft, use specprop() to get features of frequency
  ## distribution, sh and sfm, use fpeaks() to detect frequency peaks
  ## of the meanspec, use ama() and fpeaks() to detect amplitude
  ## modulations (tentative!), use dfreq() to detect dominant
  ## frequency for entire vocalization (min_freq, max_freq) OR to
  ## detect the fundamental frequency(min_f0, max_f0).
  
  ## --------------------------
  
  ## OUTPUT
  
  ## return a list of two elements
  
  ## - a dataframe with all spectro temporal features of the
  ## vocalization, along with the arguments used to detect them
  
  ## - a dataframe of two columns: file and error type in case the
  ## file could not be analysed.
  
  ## --------------------------
  
  ## initialize "error" 
  error <- ""
  
  ## initialise "res_voc" (if everything is all right)
  origin <- tools::file_path_sans_ext(wav_file_name)
  catn(glue("Working on file: ", origin))
  res_voc <-  data.frame()  
  
  ## initialise list failed
  failed <- data.frame()
  
  ## initialise "arguments"
  arguments <- data.frame(origin, min_freq, max_freq, min_dur, min_f0,
                          max_f0, wl_fft_analysis, ov_fft, wl_env, ov_env,
                          amp_th, security)
  
  ## get wav_file
  wav_file <- file.path(path_to_data, wav_file_name)
  
  ## load sound and channel
  sound <- readWave(wav_file, from = 0, to = Inf, units = "seconds")
  
  if(MONO_LEFT){
    sound <- mono(sound, "left")
    channel <- 'left'
  }else{
    sound <- mono(sound, "right")
    channel <- 'right'
    
  }
  
  sound <- addsilw(sound, sound@samp.rate, at = "end", d = security, plot = F,
                   output = "Wave")
  sound <- addsilw(sound, sound@samp.rate, at = "start", d = security, plot = F,
                   output = "Wave")
  
  sound_filtered <- ffilter(sound,custom = NULL,from = min_freq,
                            to = max_freq, wl = wl_fft_analysis, ovlp = ov_fft,
                            wn = "hanning", fftw = TRUE,
                            output="Wave")
  
  if(show_steps()){
    catn('Showsteps On! Read in the console and look at the plots - press [enter] to continue')    
    catn(glue("Bleat filtered between [min_freq:max_freq]=[{min_freq}:{max_freq}] Hz"))
    pause()
    
    catn('Oscillogram - Amplitude as a function of time')
    oscillo(sound_filtered, title = "Oscillogram")
    pause()
    
    catn('Envelop - |Amplitude| used to get the amplitude and duration')
    env(sound_filtered, envt = 'abs', plot = show_steps(), f = sound_filtered@samp.rate, title = "Envelop")
    pause()
     ## keep info show steps or not
  show_steps_tmp <- show_steps()
    catn(glue('Spectrogram between [min_freq:max_freq] = [{min_freq}:{max_freq}], with FFT using wl_fft_spectro = {get_fft_wl_spectro()} - Frequency as a function of time - the more red the color the louder'))
    spectro(sound_filtered, wl = get_fft_wl_spectro(), ovlp = 50, f=sound_filtered@samp.rate, 
            flim=c(min_freq/1000,max_freq/1000),
            collevels=seq(-60,0,1), scale = F, grid = FALSE,
            main = paste("Spectrogram - wl=", get_fft_wl_spectro(),  sep = ''))
    catn("Change spectrogram setting using set_fft_wl_spectro()")
    pause()    
    
    if(path_to_player != ''){
      listen <- prompt_yn(prompt = "Listen to the call? [Y]es or [N]o: ")
      if(listen){
        listen(sound, sound@samp.rate)
      }
    }
    dev.off()
  }
  
  ## VOC DETECTION AND DURATION
  tryCatch(
    {    
      
      ## detect call in sound to compute duration
      detect <- timer(sound_filtered, f = sound_filtered@samp.rate,
                      threshold = amp_th, dmin = min_dur,
                      power = 2, msmooth = c(wl_env,ov_env),
                      plot = show_steps(), main = 'Vocalization duration')
      
      voc_dur <- max(detect$s)
      real_start_voc <- detect$s.start[which(detect$s == voc_dur)] - security # remove security added with addsil
      real_end_voc <- real_start_voc + voc_dur
      
      if(show_steps()){
        catn(glue("Detection of vocalization duration using contour of envelop, 
                  based on amplitude threshold amp_th={amp_th}, window length wl_env={wl_env}pts & overlap between windows ov_env={ov_env}%"))
        catn("Keeping the longest segment (if several)")
        pause()
      }
      
      
      ## load sound and channel with real start and end
      voc <- readWave(wav_file, from = real_start_voc,
                      to = real_end_voc, units = "seconds")
      
      
      if(MONO_LEFT){
        voc <- mono(voc, "left")
        channel <- 'left'
      }else{
        voc <- mono(voc, "right")
        channel <- 'right'
      }
      
      ## compute envelop to get the rms
      env_voc <- env(voc, envt = 'abs', plot = F, f = voc@samp.rate)
      rms_env_voc <- rms(env_voc)
      
    },
    error = function(cond) {
      error <<- 'call not detected by timer' 
    }
  )
  
  if(error != ""){
    catn('ALERT ERROR! Analysis stopped')
    catn(error)
    catn("File name stored in a 'failed' dataframe")
    failed <- data.frame(origin)
    failed['error'] <- error
    out <- list(res_voc, failed)
    names(out) <- c('acoustic_features', 'failed_voc')
    return(out)
  }
  
  
  ## FREQUENCY DISTRIBUTION
  tryCatch(
    {
      
      ## filter and normalize sound
      voc_filtered <- ffilter(voc,custom = NULL, f = voc@samp.rate, from = min_freq, to = max_freq, wl = wl_fft_analysis, ovlp = ov_fft, wn = "hanning", fftw = TRUE, output="Wave")
      
      ## normalize sound
      voc_filtered_norm <- voc_filtered@left/max(abs(voc_filtered@left))
      
      meanspec_norm_voc <- meanspec(voc_filtered_norm, voc_filtered@samp.rate,
                                    wl = wl_fft_analysis, ovlp = ov_fft, flim = c(min_freq/1000, max_freq/1000), alim = c(0, 1) ,
                                    norm = T, plot= show_steps(), main = paste('Meanspectrum - wl=', wl_fft_analysis, sep = ''))
      if(show_steps()){
        catn("Meanspectrum=frequency distribution")
        catn(glue("Computing meanspectrum using FFT with wl_fft={wl_fft_analysis}, ov_fft={ov_fft} & [min_freq:max_freq]=[{min_freq}:{max_freq}]"))
        catn("Y axis is the relative amplitude, so between 0 and 1")
        pause()
      }
      L <- length(meanspec_norm_voc[,1])
      cumamp <- cumsum(meanspec_norm_voc[,2]/sum(meanspec_norm_voc[,2]))
      freq <- meanspec_norm_voc[,1]
      Q10 <- freq[length(cumamp[cumamp <= 0.10]) + 1]
      Q90 <- freq[length(cumamp[cumamp <= 0.90]) + 1]
      
      ## meanspec parameters
      spec_prop_voc <- specprop(meanspec_norm_voc, f = voc_filtered@samp.rate,
                                flim = c(min_freq/1000, max_freq/1000),plot = show_steps(), type = "l", col.quartiles = 4)
      if(show_steps()){
        catn("Probability density of frequency distribution from specprop()")
        catn("Extracted frequency distribution features (quartiles) + shape of spectrum (kurtosis adn skewness)")
        catn("Extracted noise parameters sh=Shannon Entropy and sfm=Spectral Flatness=Weiner Entropy.
             The closer to 1 to noisiest")
        pause()
      }
      
      
      Q25 <- spec_prop_voc$Q25
      Q50 <- spec_prop_voc$median
      Q75 <- spec_prop_voc$Q75
      IQR <- spec_prop_voc$IQR
      sd <- spec_prop_voc$sd
      mode <- spec_prop_voc$mode
      centroid <- spec_prop_voc$cent
      mean <- spec_prop_voc$cent
      sfm <- spec_prop_voc$sfm
      sh <- spec_prop_voc$sh
      kurtosis <- spec_prop_voc$kurtosis
      skewness <- spec_prop_voc$skewness
      prec <- spec_prop_voc$prec
      
      
      fpeaks_voc <- data.frame(fpeaks(meanspec_norm_voc, f = voc_filtered@samp.rate, xlim = c(min_freq/1000,max_freq/1000),threshold = 0.1, amp = c(0.01, 0.01),freq = 150, plot = show_steps()))
      
      ## FREQUENCY PEAKS
      if(show_steps()){
        catn(glue("Searching for peaks on mean frequency spectrum based between [min_freq:max_freq]=[{min_freq}:{max_freq}"))
        catn("Frequency peaks with less than 150 Hz between them are discarded. Peaks below amp_th=0.1 are discarded")
        catn("The slope of the peak matters: only peaks with a high enough slope are detected (amp = 0.01/0.01)")
        catn(glue("All peaks below 0.1 are discarded"))
        pause()
      }
      
      d <- diff(fpeaks_voc[,1], differences=2)
      harmonicity <- abs(sum(d))
      
      ## the lowest frequency peak should be the f0
      n_peaks <- nrow(fpeaks_voc)
      lowest_f_peak <- fpeaks_voc$freq[1]
      loudest_f_peak <- fpeaks_voc$freq[which(fpeaks_voc$amp == max(fpeaks_voc$amp))]
      highest_f_peak <- fpeaks_voc$freq[n_peaks]
      
      fpeaks_voc$diff_f_peaks <- NA
      if(n_peaks > 1){
        for (i in 2: n_peaks){
          fpeaks_voc$diff_f_peaks[i] <- fpeaks_voc$freq[i] - fpeaks_voc$freq[i-1]
        }
      }
      
      diff_f_peaks_mean <- mean(fpeaks_voc$diff_f_peaks, na.rm = T)
      diff_f_peaks_sd <- sd(fpeaks_voc$diff_f_peaks, na.rm = T)
      
      
      if(show_steps()){
        catn("Peaks extracted: lowest (could be f0=the fundamental frequency=lowest frequency), highest, loudest + number of peaks + frequency difference between peaks")
        catn("If the vocalisation is harmonic, then each peak is an harmonic and is always a multiple of f0. So the frequency difference between peaks may be the frequency between two harmonics and this f0")
        catn("Harmonicity is computed using diff() function. Highly harmonic vocalisation have harmonicity close to 0.")
        pause()
      }
    },
    error = function(cond) {
      error <<- paste(error,'pb meanspec', collapse = ' ')
    }
  )
  
  if(error != ""){
    catn('ALERT ERROR! Analysis stopped')
    catn(error)
    catn("File stored in a 'failed' dataframe")
    failed <- data.frame(origin)
    failed['error'] <- error
    out <- list(res_voc, failed)
    names(out) <- c('acoustic_features', 'failed_voc')
    return(out)
  }
  
  
  # compute amplitude modulation
  # tryCatch(
  #   {
  #     amp_mod_voc <- ama(voc_filtered, envt = "hil",wl = wl_fft_analysis, plot = FALSE,
  #                        flim = c(min_freq/1000, max_freq/1000), norm = T,
  #                        f = voc_filtered@samp.rate)
  # 
  #     npt_max <- min_freq*wl_fft_analysis/voc_filtered@samp.rate
  #     npt_min <- max_freq*wl_fft_analysis/voc_filtered@samp.rate
  #     
  #     amp_mod_peaks <- data.frame(fpeaks(amp_mod_voc[npt_min:npt_max,], 
  #                                        f = voc_filtered@samp.rate, 
  #                                        threshold = 0.05, amp = c(0.01,0.01), 
  #                                        plot = show_steps()))
  #     if(show_steps()) {
  #       catn("Computing amplitude modulation using ama() + fpeaks()")
  #       catn("Showing fpeaks() plot using ama() output, threshold=0.05, amp=c(0.01,0.01)")
  #       catn("Keeping the highest peak (if any): the frequency=amp_mod_rate and its amplitude=amp_mod_var")
  #       pause()
  #     }
  #     
  #     if(nrow(amp_mod_peaks) == 0){
  #       amp_mod_rate <- NA
  #       amp_mod_var <- NA
  #     }else {
  #       amp_mod_var <- max(amp_mod_peaks$amp)
  #       amp_mod_rate <- amp_mod_peaks$freq[which(amp_mod_peaks$amp == amp_mod_var)]
  #     }
  #   },
  #   error = function(cond) {
  #     error <<- paste(error,'pb amplitude modulation', collapse = ' ')
  #   }
  # )
  # 
  # if(error != ""){
  #   catn('ALERT ERROR!Analysis stopped')
  #   catn(error)
  #   catn("Vocalisation stored in a 'failed' dataframe")
  #   failed <- data.frame(origin)
  #   failed['error'] <- error
  #   out <- list(res_voc, failed)
  #   names(out) <- c('acoustic_features', 'failed_voc')
  #   return(out)
  # }
  
  ## DOMINANT FREQUENCY
  tryCatch(
    {  
      
      dominant_freq_detect <- data.frame(dfreq(voc_filtered, f = voc_filtered@samp.rate,
                                               channel = 1,
                                               wl = wl_fft_analysis,
                                               wn = "hanning",
                                               ovlp = ov_fft,
                                               threshold = amp_th,
                                               bandpass = c(min_freq,max_freq),
                                               clip = 0.1,
                                               plot = FALSE,
                                               xlab = "Times (s)",
                                               ylim = c(0, max_freq/1000),
                                               xlim = c(0, voc_dur),
                                               ylab = "Frequency (kHz)"))
      
      
      
      dominant_freq <- dominant_freq_detect[complete.cases(dominant_freq_detect$y),]
      dominant_freq <- dominant_freq[dominant_freq$y > min_f0/1000,]
      dominant_freq_n <- nrow(dominant_freq)
      
      if(dominant_freq_n > 0){
        dominant_freq_mean <- mean(dominant_freq$y)*1000
        dominant_freq_sd <- sd(dominant_freq$y)
        dominant_freq_max <- max(dominant_freq$y)*1000
        dominant_freq_min <- min(dominant_freq$y)*1000
      }else{
        dominant_freq_mean <- NA
        dominant_freq_sd <- NA
        dominant_freq_max <- NA
        dominant_freq_min <- NA
        
      }
      
      if(show_steps()) {
        spectro(voc_filtered,wl = get_fft_wl_spectro(), ovlp = 50, f=sound_filtered@samp.rate,
                flim=c(min_freq/1000,max_freq/1000),collevels=seq(-60,0,1),
                fastdisp = get_fast_display_spectro(), grid = FALSE,
                scale = F, main = paste("Spectrogram - wl =", get_fft_wl_spectro()))
        points(dominant_freq, col = 'darkred', pch = 19)
        spectroPlot <- recordPlot()
        catn(glue("Computing dominant frequency with dfreq() using a sliding window of wl={wl_fft_analysis} and overlap of ov={ov_fft}.
                  Discard if amplitude is below threshold={amp_th}"))
        catn("Plot showing the value over the duration of the signal. Extracting: mean, sd, range (=max-min) and number of points")
        pause()
      }
      
    },    error = function(cond) {
      error <<- paste(error,'pb dominant frequency', collapse = ' ')
    }
  )
  
  if(error != ""){
    catn('ALERT ERROR!Analysis stopped')
    catn(error)
    print("File stored in a 'failed' dataframe")
    failed <- data.frame(origin)
    failed['error'] <- error
    out <- list(res_voc, failed)
    names(out) <- c('acoustic_features', 'failed_voc')
    return(out)
  }
  
  ## FUNDAMENTALE FREQUENCY
  tryCatch(
    {
      ## filter the sound within a range of f0
      voc_filtered_f0 <- ffilter(voc,custom = NULL,from = min_f0, to = max_f0, wl = wl_fft_analysis, ovlp = ov_fft, wn = "hanning", fftw = TRUE, output="Wave")
      
      if(show_steps()) {
        catn("Computing fundamental frequency")
        catn(glue("Using dfreq() after filtering within [min_f0:max_f0]=[{min_f0}:{max_f0}]Hz and using a sliding window of wl={wl_fft_analysis} and overlap of ov={ov_fft}"))
      }
      
      fund_freq_detect <- data.frame(dfreq(voc_filtered_f0, f = voc_filtered_f0@samp.rate,
                                           channel = 1, wl = wl_fft_analysis,
                                           wn = "hanning",
                                           ovlp = ov_fft,
                                           threshold = amp_th,
                                           bandpass = c(min_f0,max_f0),
                                           clip = 0.05,
                                           plot = FALSE,
                                           xlab = "Times (s)",
                                           ylim = c(0, max_f0/1000),
                                           ylab = "Frequency (kHz)"))
      
      # fund_freq_detect <- autoc(voc_filtered, f = voc_filtered_f0@samp.rate, channel = 1, wl = wl_fft_analysis, fmin = min_f0, fmax = max_f0, threshold = NULL, plot = TRUE, xlab = "Time (s)", ylab = "Frequency (kHz)", ylim = c(min_freq/1000, max_freq/1000))
      
      fund_freq <- fund_freq_detect[complete.cases(fund_freq_detect$y),]
      fund_freq <- fund_freq[fund_freq$y > min_f0/1000,]
      fund_freq <- fund_freq[fund_freq$y < max_f0/1000,]
      fund_freq_n <- nrow(fund_freq)
      
      if(fund_freq_n > 0){
        fund_freq_mean <- mean(fund_freq$y, na.rm = T)*1000
        fund_freq_sd <- sd(fund_freq$y, na.rm = T)
        fund_freq_max <- max(fund_freq$y)*1000
        fund_freq_min <- min(fund_freq$y)*1000
      }else{
        fund_freq_mean <- NA
        fund_freq_sd <- NA
        fund_freq_max <- NA
        fund_freq_min <- NA
      }
      
      if(show_steps()){
        replayPlot(spectroPlot)
        points(fund_freq, col = 'black', pch = 19)
        catn(glue("Computing fundamental frequency of each FFT window based on wl_fft={wl_fft_analysis} & [min_f0:max_f0]=[{min_f0}:{max_f0}]Hz, see black points"))
        catn("When no measure could be done for a given window then no points")
        catn("Plot showing the value over the duration of the signal. Extracting: mean, sd, range (=max-min) and number of points")
        pause()
        ## dev.off()
      }
      
    },     error = function(cond) {
      error <<- paste(error,'pb fundamental frequency', collapse = ' ')
    }
  )
  if(error != ""){
    print('ALERT ERROR!Analysis stopped')
    print(error)
    print("File stored in a 'failed' dataframe")
    failed <- data.frame(origin)
    failed['error'] <- error
    out <- list(res_voc, failed)
    names(out) <- c('acoustic_features', 'failed_voc')
    return(out)
  }
  
  
  ## FORMANTS
  if(formants) {
    
    if(length(formants_bands) == 1) {
      if(show_steps()) {
        catn(glue("Formants bands not provided"))
        catn(glue("Searching for {formants_bands} formants bands"))
        pause()
      }
      formant_limists_data <- get_formants_limits_sound(voc_filtered = voc_filtered, 
                                                        origin = origin, 
                                                        min_freq_formants = max_f0, 
                                                        max_freq_formants = max_freq,
                                                        wl_fft_formants = wl_fft_formants, 
                                                        ov_fft = ov_fft,
                                                        nb_formants = formants_bands)
      formants_limits_data <- formant_limists_data$formants_limits
      
      limits <- c()
      for(f in 1:formants_bands) {
        formant_limit_name <- paste('F', f, '.upper', sep= '')
        limits <- c(limits, formants_limits_data[,formant_limit_name])
      }
    } else {
      if(show_steps()) {
        catn(glue("Formants bands provided"))
        catn(glue("Searching for {length(formants_bands)} formants features in between bands {formants_bands}Hz"))
        pause()
      }
      limits <- formants_bands
    }

    res_formants <- get_formants(origin = origin, voc_filtered = voc_filtered, 
                                 min_freq = max_f0, max_freq = max_freq, 
                                 wl_fft_formants = wl_fft_formants,
                                 wl_fft_analysis = wl_fft_analysis,
                                 ov_fft = ov_fft, amp_th = amp_th, 
                                 formants_bands = limits)
    
    formants <- res_formants$formants
    
    if(nrow(res_formants$failed_voc) >0) {
      print('ALERT ERROR!Analysis stopped')
      print("File stored in a 'failed' dataframe")
      failed <- data.frame(origin)
      failed['error'] <- 'pb extracting formants'
      out <- list(res_voc, failed)
      names(out) <- c('acoustic_features', 'failed_voc')
      return(out)
    }
    
  }
  
  
  ## if no error at all, then we build up the file
  res_voc <- data.frame(origin)
  res_voc['duration'] <- voc_dur
  res_voc['env_rms'] <- rms_env_voc
  res_voc['Q10_Hz'] <- Q10*1000
  res_voc['Q25_Hz'] <- Q25
  res_voc['Q50_Hz'] <- Q50
  res_voc['Q75_Hz'] <- Q75
  res_voc['Q90_Hz'] <- Q90*1000
  res_voc['IQR_Hz'] <- IQR
  res_voc['centroid_Hz'] <- centroid
  res_voc['sd_Hz'] <- sd
  res_voc['sh'] <- sh
  res_voc['sfm'] <- sfm
  res_voc['kurtosis'] <- kurtosis
  res_voc['skewness'] <- skewness
  res_voc['precision'] <- prec
  
  res_voc['fpeaks_npeaks'] <- n_peaks
  res_voc['fpeaks_lowest_Hz'] <- lowest_f_peak*1000
  res_voc['fpeaks_highest_Hz'] <- highest_f_peak*1000
  res_voc['fpeaks_loudest_Hz'] <- loudest_f_peak*1000
  res_voc['fpeaks_diff_mean_Hz'] <- diff_f_peaks_mean*1000
  res_voc['fpeaks_diff_sd'] <- diff_f_peaks_sd
  res_voc['harmonicity'] <- harmonicity
  res_voc['dom_freq_n_measures'] <- dominant_freq_n
  res_voc['dom_freq_mean_Hz'] <- dominant_freq_mean
  res_voc['dom_freq_sd'] <- dominant_freq_sd
  res_voc['dom_freq_range_Hz'] <- (dominant_freq_max - dominant_freq_min)
  
  res_voc['f0_n_measures'] <- fund_freq_n
  res_voc['f0_mean_Hz'] <- fund_freq_mean
  res_voc['f0_sd'] <- fund_freq_sd
  res_voc['f0_range_Hz'] <- (fund_freq_max-fund_freq_min)
  
  ## res_voc['amp_mod_var'] <- amp_mod_var
  ## res_voc['amp_mod_rate'] <- amp_mod_rate
  
  res_voc <- merge(res_voc, arguments, by = intersect(names(res_voc), names(arguments)))
  
  if(!is.null(formants_bands) & nrow(formants) > 0) {
    res_voc <- merge(res_voc, formants, by = intersect(names(res_voc), names(formants)))
  }
  
  if(show_steps()){
    catn("All spectro temporal features stored in a table")
    catn("The arguments used in the function are also stored")
    print(res_voc)
    catn("It is a looooot a parameters... let's see what to do with them!")
    pause()
  }
  
  ## return results on voc
  out <- list(res_voc, failed)
  names(out) <- c('acoustic_features', 'failed_voc')
  return(out)
}

if(ok_globals == TRUE){
  catn("Functions loaded: baaacoustics_dir() and baaacoustics(path_to_data, wav_file_name, path_to_analyses, min_freq, max_freq, min_dur, min_f0, max_f0, wl_fft_analysis, wl_fft_formants, ov_fft, wl_env, ov_env, amp_th)")
  catn(glue("MONO_LEFT = {MONO_LEFT}", ))
}else{
  catn("Functions not loaded. To load the them properly, please define global variables: MONO_LEFT")
}




