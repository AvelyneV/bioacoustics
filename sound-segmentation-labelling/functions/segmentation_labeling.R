## ------/!\------ ##
## /!\ WARNING /!\
## ------/!\------ ##
##
## Global variables:
## WL: window length for FFT
## WAVE_EXTENSION: string wave files extension
## SHOW_STEPS: TRUE OR FALSE to show key steps
## FS: Frequency sampling
##
## ------------------------
globals <- c("WAVE_EXTENSION", 'WL','SHOW_STEPS')
ok_globals <- TRUE
for (g in globals) {
  if (!exists(g)) {
    print("======= ALERT =======")
    print(sprintf("The variable '%s' is not defined.", g))
    print("This is not gonna work.")
    print("=====================")
    ok_globals <- FALSE
  }
}

library(seewave)
library(tuneR)

## --------------------------
## --- SET UP ENVIRONMENT ---
## --------------------------

directories_automated_segmentation_labeling <- function(path_to_raw_data, path_to_output_data, semi_manual_labels){
  
  if(semi_manual_labels == TRUE){
  output_folder <- file.path(path_to_output_data, 'output_semi_manual_segmentation_labeling')
  }else{
    output_folder <- file.path(path_to_output_data, 'output_auto_segmentation_labeling')
  }
  
  if (!dir.exists(output_folder)) {
    dir.create(output_folder)
  }
  
  label_tracks_folder <- file.path(output_folder, "label_tracks")
  if (!dir.exists(label_tracks_folder)) {
    dir.create(label_tracks_folder)
  }
  
  log_file <- file.path(output_folder, "log.tsv")
  if(!file.exists(log_file)) {
    log <- data.frame(matrix(ncol = 12, nrow = 0))
    columns <- c('origin','min_freq', 'max_freq', 'threshold', 'power', 'env_window_length', 'env_window_ov', 'min_dur', 'segment_label','security', 'segments', 'segmentation_error')
    colnames(log) <- columns
    write.table(log, log_file, sep = '\t', row.names = FALSE, quote = FALSE)
  }
  if(SHOW_STEPS){
    print("Showsteps on! Output folder created in 'path_to_output_data'. It contains")
    print(dir(output_folder))
    readline(prompt = "Press [enter] to continue")
  }
  return(list(output_folder, label_tracks_folder, log_file))
}

## ---------------------------------------------
## --- SEGMENTATION AND LABELING OF ONE FILE ---
## ---------------------------------------------
segmentation_labeling_file <- function(path_to_raw_data, path_to_output_data, wav_file, threshold, power, min_freq, max_freq, env_window_length, env_window_ov, min_dur, security, segment_label, accepted_labels, semi_manual_labels = semi_manual_labels){
  
  ## -----------------------------------
  
  ## Arguments
  
  ## path_to_raw_data = path to directory with WAVE_EXTENSION to run
  
  ## path_to_output_data = path to directory where the 'output' folder
  ## will be created
  
  ## wav_file = string, name of WAVE_EXTENSION file contained in the
  ## path_to_raw_data folder
  
  ## threshold = numerical value, percentage for amplitude detection
  
  ## power = numerical value from 1 to... ? the higher the more
  ## decreasing the impact of the background noise but the shorter the
  ## detections in segmentation process
  
  ## min_freq and max_freq = the min and max frequencies the sound
  ## will ne filtered before segmentation
  
  ## env_window_length = a numerical value, the window length of the
  ## computed envelop for detection (in points, not in seconds)
  
  ## env_window_ov = numerical, percentage of overlap between two
  ## sliding window of envelop computation
  
  ## min_dur = the minimum duration for detected segments
  
  ## segment_label = the default label of detected segments
  
  ## security = time added around the segment boundaries (in %)
  
  ## if semi_manual_labels = T: accepted_labels = a vector of strings with the accepted label to
  ## be added during the labeling process. By default, 't' and 'T' are added for trashing
  
  ## if semi_manual_labels = TRUE OR FALSE. IF TRUE, run the semi_manual_labeling() function. 
  
  ## ----------------------------
  
  ## Actions
  
  ## 1) if first time run, creates 'output' directory (and subsequent
  ## directories: 'label_tracks', 'segments', 'backgrounds') and a
  ## 'log.tsv' file to monitor segmentation success.
  
  ## 2) Extract segments from file with or without manual editing.
  
  ## ----------------------------
  
  ## Output
  
  ## a dataframe with the updated log containing segmentation
  ## parameters that were chosen.
  
  ## For each file a label track is created in label_tracks folder. Contains start (with security), end (with security), label, index, security value for segment, file name of wav_file
  
  ## ----------------------------   
  
  ## set up environment
  output <- directories_automated_segmentation_labeling(path_to_raw_data, path_to_output_data, semi_manual_labels = semi_manual_labels)
  output_automated_segmentation_labeling <- output[[1]]
  label_tracks <- output[[2]]
  log_file <- output[[3]]
  
  ## get file name
  origin <- tools::file_path_sans_ext(wav_file)
  print(paste0("Working on file: ", origin))
  
  ## load the rec
  sound_file <- file.path(path_to_raw_data, wav_file)
  sound <- readWave(sound_file,from = 0, unit='seconds')
  
  ## get the frequency sampling
  FS <- sound@samp.rate
  
  ## detect segment in stereo file
  sound <- mono(sound, 'left')
  sound_filtered <- ffilter(sound,custom = NULL, from = min_freq, to = max_freq, wl = WL, ovlp = 50, wn = "hanning", fftw = FALSE, output="Wave")
  
  sound_filtered_norm <- sound_filtered@left/max(abs(sound_filtered@left))
  
  ## print step
  if(SHOW_STEPS){
    print(paste0("Showsteps on! Sound filtered, bandwidth: [min_freq=", min_freq, ":max_freq=", max_freq, "] Hz, with hanning method, wl=", WL," ov=50%"))
    print("Sound normalized")
    readline(prompt = "Press [Enter] to continue")
  }
  
  if(semi_manual_labels == FALSE){
    ## case to directly detect without showing nor editing the segmentation --> get sound_detect
    error <- ""
    tryCatch(
      {
        sound_detect <-timer(sound_filtered_norm, msmooth = c(env_window_length,env_window_ov), threshold = threshold , power = power, dmin = min_dur, plot = F, f= FS, main = origin)
      }
      , 
      error = function(cond){
        error <<- 'no detection in file'
      }
    )
    
    if(error == ""){
      ## get the label track if no errors in timer
      segments <- length(sound_detect$s.start)
      real_security <- (security/100)*sound_detect$s
      start <- sound_detect$s.start - real_security
      end <- sound_detect$s.end + real_security
      tab <- data.frame(start, end)
      tab['label'] <- segment_label
      tab['index'] <- c(1:segments)
      tab['security'] <- real_security
      tab['origin'] <- origin
      
      ## print step
      if(SHOW_STEPS){
        print(paste0("Showsteps on! ", segments , " detected here is the label track table:"))
        print(tab)
        readline(prompt = "Press [Enter] to continue")
      }
    }else{
      print(error)
      ## export empty label track if error in timer
      segments <- 0
      tab <- data.frame(matrix(ncol = 6, nrow = 0))
      tab[1,] <- c('NA', 'NA', 'NA', 'NA', 'NA', origin)
      colnames(tab) <- c('start', 'end', 'label', 'index', 'security', 'origin')
      
      ## print step
      if(SHOW_STEPS){
        print(paste0("Showsteps on! ", segments , " detected here is the label track table:"))
        print(tab)
        readline(prompt = "Press [Enter] to continue")
      }
    }      
  }else{
    ## case to show the double window with spectrogram and detection to edit labels --> get sound_detect
    
    ## print step
    if(SHOW_STEPS){
      print(paste0("Showsteps on! You chose to visualize and edit labels:"))
      readline(prompt = "Press [Enter] to continue")
    }
    
    labeling <- semi_manual_labeling(sound, 
                                     origin, 
                                     fs = FS, 
                                     threshold, 
                                     power, 
                                     min_freq, 
                                     max_freq, 
                                     env_window_length, 
                                     env_window_ov, 
                                     min_dur, 
                                     segment_label, 
                                     accepted_labels)
    
    durations <- labeling[[2]]
    tab <- labeling[[1]]
    error <- labeling[[3]]
    
    if(nchar(error) != 0){
      ## export empty label track table if error in timer or control
      segments <- 0
      tab <- data.frame(matrix(ncol = 6, nrow = 0))
      tab[1,] <- c('NA', 'NA', 'NA', 'NA', 'NA', origin)
      colnames(tab) <- c('start', 'end', 'label', 'index', 'security', 'origin')
      
      ## show step
      if(SHOW_STEPS){
        print(paste0(error, " : empty label track table"))
        print(tab)
        readline(prompt = "Press [Enter] to continue")
      }
    }else{
      ## empty full label track table 
      real_security <- (security/100)*durations
      segments <- length(durations)
      tab$start <- tab$start - real_security
      tab$end <- tab$start + real_security
      tab['security'] <- real_security
      tab['origin'] <- origin
      
      ## show step
      if(SHOW_STEPS){
        print(paste0("Showsteps on! ", segments , " detected here is the label track table:"))
        print(tab)
        readline(prompt = "Press [Enter] to continue")
      }
    }
  }
  
  ## export label track in stereo folder
  label_track_name <- paste(origin, 'auto.labels.txt', sep = '.')
  label_track_file <- file.path(label_tracks , label_track_name)
  write.table(tab,label_track_file, row.names = F, sep = '\t', quote = F)
  
  ## save segmentation parameters in log
  seg_param <- data.frame(origin)
  seg_param['min_freq'] <- min_freq
  seg_param['max_freq'] <- max_freq
  seg_param['threshold'] <- threshold
  seg_param['power'] <- power
  seg_param['env_window_length'] <- env_window_length
  seg_param['env_window_ov'] <- env_window_ov
  seg_param['min_dur'] <- min_dur
  seg_param['security'] <- security
  seg_param['segments'] <- segments
  
  if(error == ""){
    comment_error <- 'NA'
  }else{
    comment_error <- error
  }
  seg_param['segmentation_error'] <- comment_error
  
  ## show step
  if(SHOW_STEPS){
    print(paste0("Showsteps on! The segmentation log of this file"))
    print(seg_param)
    readline(prompt = "Press [Enter] to continue")
  }
  
  return(seg_param)
}


## ----------------------------------------
## --- SEGMENTATION AND LABELING on DIR ---
## ----------------------------------------
segmentation_labeling_dir<- function(path_to_raw_data, path_to_output_data, threshold, power, min_freq, max_freq, env_window_length, env_window_ov, min_dur, security, segment_label, accepted_labels, semi_manual_labels = semi_manual_labels){
  ## -----------------------------------
  
  ## Arguments
  
  ## path_to_raw_data = path to directory with WAVE_EXTENSION to run
  
  ## path_to_output_data = path to directory where the 'output' folder
  ## will be created
  
  ## threshold = numerical value, percentage for amplitude detection
  
  ## power = numerical value from 1 to... ? the higher the more
  ## decreasing the impact of the background noise but the shorter the
  ## detections in segmentation process
  
  ## min_freq and max_freq = the min and max frequencies the sound
  ## will ne filtered before segmentation
  
  ## env_window_length = a numerical value, the window length of the
  ## computed envelop for detection (in points, not in seconds)
  
  ## env_window_ov = numerical, percentage of overlap between two
  ## sliding window of envelop computation
  
  ## min_dur = the minimum duration for detected segments
  
  ## segment_label = the default label of detected segments
  
  ## security = time added around the segment boundaries (in %)
  
  ## if semi_manual_labels = T: accepted_labels = a vector of strings with the accepted label to
  ## be added during the labeling process. By default, 't' and 'T' are added for trashing
  
  ## if semi_manual_labels = TRUE OR FALSE. IF TRUE, then a plot window allow to edit the labeling file after file. option continue or quit segmentation after each file 
  
  ## ----------------------------
  
  ## Actions
  
  ## 1) if first time run, creates 'output' directory (and subsequent
  ## directories: 'label_tracks', 'segments', 'backgrounds') and a
  ## 'log.tsv' file to monitor segmentation success.
  
  ## 2) Will check which files were already segmented in the directory and will discard them
  
  ## 3) for each file, run the segmentation_labeling_dir() function
  
  ## ----------------------------
  
  ## Output
  
  ## a dataframe with the updated log containing segmentation
  ## parameters that were chosen.
  
  ## For each file a label track is created in label_tracks folder. Contains start (with security), end (with security), label, index, security value for segment, file name of wav_file
  
  ## ----------------------------   
  
  ## set up environment
  output <- directories_automated_segmentation_labeling(path_to_raw_data, path_to_output_data, semi_manual_labels)
  output_automated_segmentation_labeling <- output[[1]]
  label_tracks <- output[[2]]
  log_file <- output[[3]]
  
  ## skip files already in log
  log <- read.table(log_file, h = T, sep = '\t')
  done_files <- paste(log$origin, WAVE_EXTENSION, sep ='.')
  if(length(done_files)>0){
    wav_list = dir(path_to_raw_data, WAVE_EXTENSION)
    wav_list = setdiff(wav_list,done_files)
  }else{
    wav_list = dir(path_to_raw_data, WAVE_EXTENSION)
  }
  
  ## show if wav_list if empty...
  if(length(wav_list) == 0){
    print("It looks like the list of wav files is empty, maybe you did a segmentation already on all files? Cannot redo them")
  }
  
  if(SHOW_STEPS){
    print(paste0("Showsteps ON! ", length(done_files), " files of the directory already segmented, skipped. List of skipped:"))
    print(done_files)
    print(paste0("Showsteps on! List of file the function will go through: "))
    print(wav_list)
    readline(prompt = "Press [enter] to continue: ")
  }
  
  ## loop on all directory
  first = TRUE
  for(wav_file in wav_list){
    
    ## load log to update
    log <- read.table(log_file, h = T, sep = '\t')
    
    ## very long use of tryCatch for a simple thing: we show steps for at most
    ## one file
    tryCatch(
      {
        show_steps_bu <- SHOW_STEPS
        if (!first) {
          SHOW_STEPS <<- FALSE
          ## print("Working on all other files at once")
        }
        first <- FALSE
        
        ## here is the main call
        seg_log <- segmentation_labeling_file(path_to_raw_data, 
                                              path_to_output_data, 
                                              wav_file, 
                                              threshold, 
                                              power, 
                                              min_freq, 
                                              max_freq, 
                                              env_window_length, 
                                              env_window_ov, 
                                              min_dur, 
                                              security, 
                                              segment_label, 
                                              accepted_labels, 
                                              semi_manual_labels = semi_manual_labels)
      },
      error = function(cond) {
        message(cond)
        return(NA)
      },
      warning = function(cond) {
        message(cond)
        return(NA)
      },
      finally = {
        SHOW_STEPS <<- show_steps_bu
      }
    )
    
    ## update log
    log = rbind(log, seg_log)
    
    ## save log
    write.table(log, log_file, sep='\t', row.names = F, quote = F)
    
    if(semi_manual_labels == TRUE){
      ## ask user if they want to continue with the next file or stop here
      continue = readline(prompt = "Continue with the next file? Press [enter] to continue or [Q] to quit: ")
      while(continue != 'q' & continue != 'Q' & continue != ''){
        print("You must either press [enter] or [Q]")
        continue = readline(prompt = "Continue with the next file? Press [enter] to continue or [Q] to quit: ")
      }
      stopit = (continue == 'q'| continue == 'Q')
      if(stopit == TRUE){
        break
      }
    }
  }
  
  if(SHOW_STEPS){
    print(paste0("Showsteps ON! Updated log returned by function: "))
    print(str(log))
    readline(prompt = "Press [enter] to continue: ")
  }
  return(log)
}


if(ok_globals == TRUE){
  print("Functions segmentation_labeling_dir(path_to_raw_data, path_to_output_data, threshold, power, min_freq, max_freq, env_window_length, env_window_ov, min_dur, security, segment_label, accepted_labels, semi_manual_labels = TRUE OR FALSE) and segmentation_labeling_file() loaded")
  print(paste0("WAVE_EXTENSION = ", WAVE_EXTENSION))
  print(paste0("WL = ", WL))
  print(paste0("MODE: SHOW_STEPS = ", SHOW_STEPS))
}

source('./functions/semi_manual_labeling.R')

