tuneR::setWavPlayer('/usr/bin/vlc')
## function to help segmenting and labelling sound sequences

## we need to get the filename, load the wav file, filter it,
## normalize it and extract detections segments with timer function,
## ask the user for label of each detected segments, save the
## wavefiles (without any filtering !) into a database and save the annotation files in a folder
## with the same name

print('Script with functions getSegmentationAnnotationFile(soundFolder, wavfile, threshold, power, minfreq, maxfreq, minDur, calltype) and getSegmentationAnnotationDir(soundFolder, threshold, power, minfreq, maxfreq, minDur, calltype)')
print("First the program checks if an 'outputFolder' already exists OR create it if not")
print("The 'outputFolder' contains 'call_database' + 'annotation_files'+ 'log.txt' to monitor your analysis")
print("Output = each segment labels as good voc + annotation file of sequence (with V and T for trash) and the log")



outputFolder <- file.path(path_to_species, 'output')
if (!dir.exists(outputFolder)) {
  dir.create(outputFolder)
}

callFolder <- file.path(outputFolder, "call_database")
if (!dir.exists(callFolder)) {
  dir.create(callFolder)
}

backgroundFolder <- file.path(outputFolder, "background_extracts")
if (!dir.exists(backgroundFolder)) {
  dir.create(backgroundFolder)
}

annotationFolder <- file.path(outputFolder, "annotation_files")
if (!dir.exists(annotationFolder)) {
  dir.create(annotationFolder)
}

logFile <- file.path(outputFolder, "log.txt")
if(!file.exists(logFile)) {
  log <- data.frame(matrix(ncol = 2, nrow = 0))
  columns <- c('filename', 'comment')
  colnames(log) <- columns
  write.table(log, logFile, sep = '\t', row.names = FALSE)
}

## Libraries
library(seewave)
library(tuneR)

getSegmentationAnnotationFile <- function(soundFolder, wavfile, threshold, power, minfreq, maxfreq, minDur, calltype) {
  
  print("~~~ Working on file: ~~~")
  print(wavfile)
  
  ## get filename
  filename = unlist(strsplit(wavfile,'.wav'))[1]
  
  ## check if filename is already in the log file. If yes print "already done" and break. If not run it
  logFile <- file.path(outputFolder, 'log.txt')
  log <- read.table(logFile, h = T)
  doneFiles <- paste(log$filename, '.wav', sep = '')
  if(length(grep(wavfile,doneFiles)) > 0){
    print("This file is already done: remove it from the 'log.txt' file to redo it or run another one")
  }else{
    
    ## load the wavfile
    soundFile <- file.path(soundFolder, wavfile)
    sound1=readWave(soundFile,from =0,to = 10, unit='seconds')
    sound = mono(sound1, "left")
    
    ## get the frequency sampling
    fs=sound@samp.rate
    
    ## filter
    sound_filtered <- ffilter(sound,custom = NULL,from=minfreq, to = maxfreq, wl = 1024, ovlp = 50, wn = "hanning", fftw = TRUE, output="Wave")## set up the bandwidth
    
    ## normalize sound
    sound_filtered_norm <- sound_filtered@left/max(abs(sound_filtered@left))
    
    ## split screen in two
    par(bg = "white") ## to allow erasing and ploting again
    split.screen(c(2, 1))
    
    screen(1) ## draw in first screen
    sound_filtered_spectro <- ffilter(sound,custom = NULL,from=100, to = 6000, wl = 1024, ovlp = 90, wn = "hanning", fftw = TRUE, output="Wave")
    spectro(sound_filtered_spectro,wl=1024,ovlp = 90, f=fs, flim=c(0,6),collevels=seq(-60,-5,5), scale = F)
    abline(v=9, col = 'red')
    ## text(syl,5.5,indexes, col = 'red', cex = 1)
    
    screen(2, TRUE) ## new drawing in second screen
    sound_detect <-timer(sound_filtered_norm,msmooth = c(1500,90), power = power, threshold = threshold , dmin = minDur, plot = T,f = fs, main = filename)
    
    syl = sound_detect$s.start +(0.5*sound_detect$s)
    indexes = as.character(c(1:length(sound_detect$s.start)))
    
    start = sound_detect$s.start
    end = sound_detect$s.end
    ## initiate table with default labels and indexes
    detect = data.frame(start,end)
    detect['index'] = c(1:nrow(detect))
    
    text(syl,0.9,indexes, col = 'black', cex = 1)
    
    ## option listen to the sound
    listen = readline(prompt = "Do you want to listen to the sound? (y/n) and press [enter]: ")
    while(listen != 'y' & listen != 'Y' & listen != 'n' & listen != 'N' ){
      print('You must write either y or n')
      listen = readline(prompt = "Do you want to listen to the sound?? (y/n) and press [enter]: ")}
    
    if(listen == 'Y'|listen == 'y'){
      listen(sound,fs)}
    
    ## ask user if they want to continue with the next file or stop here
    continue = readline(prompt = "Continue file [enter] or skip it ? [s]: ")
    while(continue != '' & continue != 's' & continue != 'S'){
      print("You must either press [enter] or ship 's' t skip")
      continue = readline(prompt = "Continue file [enter] or skip it ? [s]: ")
    }
    skip = (continue == 's'| continue == 'S')
    
    if(skip == TRUE){
      comment = "skipped"
      output = data.frame(filename)
      output ['comment']= comment
      print("file skipped = no annotation file was created and no calls were extracted")
      output ['power']= 'NA'
      output ['threshold']= 'NA'
      output['Ndetected']= 'NA'
      output['Nselected']='NA'
      output ['background_timeS'] ='NA'
      output ['background_durS'] = 'NA'
      return(output)
    }else{
      
      ## change power and or thresold argument
      settings = readline(prompt = "Want to change detection settings? Yes then press [y] or press [enter]: ")
      while(settings != 'y' & settings != 'Y' & settings != ''){
        print('You must write either y or press [enter] if seetings ok')
        settings = readline(prompt = "Want to change detection settings? Yes then press [y] or press [enter]: ")}
      
      goodSettings = !nzchar(settings)
      Ndetected = nrow(detect)
      while(!goodSettings){
        set = readline(prompt = "Want to change power [p] or threshold [t]? Press enter when done: ")
        goodSettings = !nzchar(set)
        
        if(!goodSettings){
          
          if(set == 'p' | set == 'P'){
            power = readline("New power argument ? ")
            power = as.numeric(power)
            while(is.na(power) == TRUE){
              print('You must write a number')
              power = readline("New power argument ? ")
              power = as.numeric(power)}
          }
          
          if(set == 't'| set == 'T'){
            threshold = readline("New threshold argument ? ")
            threshold = as.numeric(threshold)
            while(is.na(threshold) == TRUE){
              print('You must write a number')
              threshold = readline("New threshold argument ? ")
              threshold = as.numeric(threshold)}
            
          }
          screen(2, TRUE)
          sound_detect <-timer(sound_filtered_norm,msmooth = c(1500,90), power = power, threshold = threshold , dmin = 0.01, plot = T,f = fs, main = filename)
          syl = sound_detect$s.start +(0.5*sound_detect$s)
          indexes = as.character(c(1:length(sound_detect$s.start)))
          text(syl,0.9,indexes, col = 'black', cex = 1)
          
          start = sound_detect$s.start
          end = sound_detect$s.end
          ## initiate table with indexes
          detect['index'] = c(1:nrow(detect))
          
        }
      }
      
      ## ask user to select the detection segments
      select = 'init'
      okcalls = !nzchar(select)
      while(!okcalls){
        select = readline(prompt = "Enter the indexes you want to extract (separated by [space]), press enter if done: ")
        okcalls = !nzchar(select)
        if(!okcalls){
          calls = strsplit(select, ' ')[[1]]
          calls = as.numeric(calls)
          
          ## inspect new labeling
          print('Inspect labeling')
          screen(2, TRUE)
          sound_detect <-timer(sound_filtered_norm,msmooth = c(1500,90), power = power, threshold = threshold, dmin = 0.01 ,plot = T,f = fs, main = filename)
          if(length(calls)>0){
            text(syl[calls],0.9,calls, col = 'green3', cex = 1)
            text(syl[calls], 0.05, calltype, cex = 1, col= 'green3' )
          }
          
          start = sound_detect$s.start
          end = sound_detect$s.end
          ## initiate table with default labels and indexes
          detect = data.frame(start,end)
          detect['index'] = c(1:nrow(detect))
          
        }
      }
    }
    
    if(length(calls)>0){
      ## create the final annotation table with only selected calls
      for(i in 1: length(calls)){
        if(i ==1){
          detect_final = detect[calls[i],]
        }else{
          detect_final = rbind(detect_final,detect[calls[i],])
        }
      }
      detect_final['label'] = calltype
      Nselected <- nrow(detect_final)
      
      
      ## extract calls
      for(i in 1:nrow(detect_final)){
        start = detect_final$start[i] - 0.03
        end = detect_final$end[i] + 0.03
        
        start = detect_final[i,1]-0.03
        end = detect_final[i,2]+0.03
        call_extract = readWave(soundFile,from = start,to = end, unit='seconds')
        
        ## name of the call extracted
        segment_name = paste(filename,detect_final$label[i],detect_final$index[i], sep='_')
        segment_name = paste(segment_name,'.wav', sep='')
        segment_name = paste(callFolder,segment_name, sep='/')
        writeWave(call_extract, filename = segment_name)
      }
    }else{
      detect['label'] = 'NA'
      detect_final = detect[0,]
    }
    
    ## print annotation table
    print("Selected segments were extracted and saved as wavfiles see 'call_database' folder")
    
    ## save annotation file with proper filename into the annotation folder
    annotation_file_name = paste(filename, '.txt', sep='')
    annotation_file_name =paste(annotationFolder, annotation_file_name, sep='/')
    
    write.table(detect_final,annotation_file_name, sep='\t', row.names=F)
    print("annotation file is stored in the 'annotation_files' folder")
    
    ## ask if some background noise should be extracted
    background = readline(prompt = "Want to extract some background noise? Yes [y] or press [enter]: ")
    while(background != 'y' & background != 'Y' & background != '' ){
      print('You must write either y or press enter')
      background = readline(prompt = "Want to extract some background noise? Yes [y] or press [enter]: ")
    }
    
    doneBackground = !nzchar(background)
    timeBackground = c()
    durBackground = c()
    i = 1
    while(!doneBackground){
      time = readline(prompt = "Enter time to start background extraction or press [enter] if done: ")
      doneBackground = !nzchar(time)
      if(!doneBackground){
        time = as.numeric(time)
        while(is.na(time) == TRUE){
          print('You must write a number')
          time = readline(prompt = "Enter time to start background extraction: ")
          time = as.numeric(time)
        }
        
        dur = readline(prompt = "Enter duration of background extraction: ")
        dur = as.numeric(dur)
        while(is.na(dur) == TRUE){
          print('You must write a number')
          dur = readline(prompt = "Enter duration of background extraction: ")
          dur = as.numeric(dur)
        }
        
        start = time
        end = time + dur
        backgroundSound = readWave(soundFile,from = start,to = end, unit='seconds')
        
        ## name of segment
        segment_name = paste(filename, 'background', i, sep='_')
        segment_name = paste(segment_name,'.wav', sep='')
        segment_name = paste(backgroundFolder,segment_name, sep='/')
        writeWave(backgroundSound, filename = segment_name)
        i= i + 1
        timeBackground = c(timeBackground, time)
        durBackground = c(durBackground, dur)
      }
    }
    
    comment = readline(prompt = "Add a comment about this file in the log? Write and/or press [enter]: ")
    
    output = data.frame(filename)
    output ['comment']= comment 
    output ['power']= power
    output ['threshold']= threshold
    output['Ndetected']= Ndetected
    output['Nselected']= Nselected
    
    if(length(timeBackground) == 0){
      output ['background_timeS'] ='NA'
      output ['background_durS'] = 'NA'
    }else{
      output ['background_timeS'] = paste(timeBackground,collapse=';')
      output ['background_durS'] = paste(durBackground, collapse=';')
    }
    return(output)
  }
}



### Function to work on all files from a directory
getSegmentationAnnotationDir <- function(soundFolder, threshold, power, minfreq, maxfreq, minDur, calltype){
  
  ## remove files that are already in the log file
  logFile <- file.path(outputFolder, 'log.txt')
  log <- read.table(logFile, h = T)
  doneFiles <- paste(log$filename, '.WAV', sep = '')
  if(length(doneFiles)>0){
    wavList = dir(soundFolder, '.WAV')
    wavList = setdiff(wavList,doneFiles)
  }else{
    wavList = dir(soundFolder, '.WAV')
  }
  
  for(wavfile in wavList){
    
    segment_annotation = getSegmentationAnnotationFile(soundFolder, wavfile,threshold = threshold, power = power, minfreq = minfreq, maxfreq = maxfreq, minDur = minDur, calltype = calltype)
    log = rbind(log,segment_annotation)
    
    logname <- paste(outputFolder, 'log.txt', sep = '/')
    write.table(log,logname, sep='\t', row.names=F)
    
    ## ask user if they want to continue with the next file or stop here
    continue = readline(prompt = "Do you want to continue with the next file ? Press [enter] or [q] to quit: ")
    while(continue != 'q' & continue != 'Q' & continue != ''){
      print("You must press [enter] or write 'q'")
      continue = readline(prompt = "Do you want to continue with the next file ? Press [enter] or [q] to quit: ")
    }
    stopit = (continue == 'q'| continue == 'Q')
    if(stopit == TRUE){
      break
    }
  }
  return(log)
}

