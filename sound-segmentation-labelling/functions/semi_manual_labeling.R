tuneR::setWavPlayer('/usr/bin/vlc')
## ------/!\------ ##
## /!\ WARNING /!\
## ------/!\------ ##
##
## Global variables:
## WL: window length for FFT
##
## ------------------------
globals <- c('WL', 'SHOW_STEPS')
ok_globals <- TRUE
for (g in globals) {
  if (!exists(g)) {
    print("======= ALERT =======")
    print(sprintf("The variable '%s' is not defined.", g))
    print("This is not gonna work.")
    print("=====================")
    ok_globals <- FALSE
  }
}


### ----------------------------------
### --- SEMI MANUAL LABELLING FUNCTION
### ----------------------------------

semi_manual_labeling <- function(sound, file_name, fs, threshold, power, min_freq, max_freq, env_window_length, env_window_ov, min_dur, segment_label, accepted_labels){
  
  ## -----------------------------------
  
  ## Arguments
  
  ## sound = a sound file loaded.
  
  ## file_name = name of file 
  
  ## fs = frequency sampling Hz
  
  ## threshold = numerical value, percentage for amplitude detection
  
  ## power = numerical value from 1 to... ? the higher the more
  ## decreasing the impact of the background noise but the shorter the
  ## detections in segmentation process
  
  ## min_freq and max_freq = the min and max frequencies the sound
  ## will be filtered before segmentation
  
  ## env_window_length = a numerical value, the window length of the
  ## computed envelop for detection (in points, not in seconds)
  
  ## env_window_ov = numerical, percentage of overlap between two
  ## sliding window of envelop computation
  
  ## min_dur = the minimum duration for detected segments
  
  ## segment_label = the default label of detected segments
  
  ## accepted_labels = a vector of strings with the accepted label to
  ## be added during the labeling process. By default, 't' and 'T' are added for trashing
  
  ## ----------------------------
  
  ## Actions
  
  ## Open a double plot frame window. Plot the spectrogram on top and timer plot at the bottom. Ask for editing the labels.
  
  ## ----------------------------
  
  ## Output
  
  ## a list of three elements: if segments detected, a dataframe with the label track table (start, end, label, index), durations of segments, empty error string. If no segments two empty vectors, and a string with error message 
  
  ## ----------------------------   
  print(paste0("Starting the semi manual labeling for: ", file_name))

  sound_filtered <- ffilter(sound, custom = NULL, from = min_freq, to = max_freq, wl = WL, ovlp = 50, wn = "hanning", fftw = FALSE, output="Wave")
  sound_filtered_norm <- sound_filtered@left/max(abs(sound_filtered@left))
  
  ## split screen in two
  par(bg = "white") ## to allow erasing to plot again
  split.screen(c(2, 1))
  
  ## plot spectrogram of sound
  screen(1) ## draw in first screen : 
  sound_filtered_spectro <- ffilter(sound, custom = NULL, from = 0, to = 7000, wl = WL, ovlp = 90, wn = "hanning", fftw = TRUE, output="Wave")
  spectro(sound_filtered_spectro, wl = 1024, ovlp = 90, f = fs, flim=c(0,7), collevels=seq(-60,-5,5), scale = F)
  
  ## print step
  if(SHOW_STEPS){
    print(paste0("Showsteps on! Spectrogram always between 0 and 7000 Hz, hanning method, wl=", WL," ov=90%"))
    readline(prompt = "Press [Enter] to continue: ")
  }
  
  ## option listen to the sound
  listen = readline(prompt = "Listen to the sound? [Y]es or [N]o ")
  while(listen != 'y' & listen != 'Y' & listen != 'n' & listen != 'N' ){
    print('You must press either [Y] or [N]')
    listen = readline(prompt = "Listen to the sound? [Y]es or [N]o ")
  }
  if(listen == 'Y'|listen == 'y'){
    listen(sound,fs)
  }
  
  
  screen(2, TRUE) ## new drawing in second screen
  error <- ""
  tryCatch(
    {    
      plot_main <- paste0("File ", file_name, ": power=", power, ", min_dur=", min_dur, ", env_window_length=", env_window_length, ", env_window_ov=", env_window_ov)
      sound_detect <-timer(sound_filtered_norm, msmooth = c(env_window_length, env_window_ov), power = power, threshold = threshold , dmin = min_dur, plot = T,f = fs, main = plot_main)
    },
    error = function(cond) {
      error <<- 'No segments detected by timer' 
    }
  )
  ## print step
  if(SHOW_STEPS){
    print("Showsteps on! When possible, the segmentation appears bottom screen if timer managed")
    readline(prompt = "Press [Enter] to continue: ")
  } 
  
  
  if(nchar(error) == 0){
    ## good settings so create table of detection
    start = sound_detect$s.start
    end = sound_detect$s.end
    detect = data.frame(start, end)
    detect['label'] = segment_label
    detect['index'] = c(1:nrow(detect))
    
    ## inspect new labeling
    print('Inspect labeling')
    seg = sound_detect$s.start +(0.5*sound_detect$s)
    indexes = as.character(c(1:length(sound_detect$s.start)))
    text(seg,0.9, detect$index, col = 'purple', cex = 1)
    text(seg, 0.74, detect$label, cex = 1, col= 'purple' )
    
    
    ## ask user to change some labels
    correct = readline(prompt = "Accept labeling? [Y]es to continue, [N]o to edit label(s): ")
    while(correct != 'y' & correct != 'Y' & correct != 'n' & correct != 'N' ){
      print('You must write either y or n')
      correct = readline(prompt = "Accept labeling? [Y]es to continue, [N]o to edit label(s): ")
    }
    
    ## edit labels when user wants to
    enough = (correct == 'y' | correct == 'Y')
    while(!enough){
      index = readline(prompt = "Enter the index to correct, press [enter] if done: ")
      if(!nzchar(index)){
        enough = TRUE
      }
      else{
        index = as.numeric(index)
        while(is.na(index) == TRUE){
          print('You must write a number')
          index = readline(prompt = "Enter the index to correct, press [enter] if done: ")
          index = as.numeric(index)
        }
        
        new_label = readline(prompt = "Enter the new label ([T] for 'trash'): ")
        accepted = c(accepted_labels, 't', 'T')
        accepted = grep(new_label, accepted)
        while(length(accepted) == 0){
          print('You must enter an accepted label')
          new_label = readline(prompt = "Enter the new label ([T] for 'trash'): ")
          accepted = grep(new_label, accepted_labels)
        }
        
        detect$label[index] = new_label
      }
    }
    
    print('Final labeling in green')
    text(seg,0.9, detect$index, col = 'darkgreen', cex = 1)
    text(seg, 0.60, detect$label, cex = 1, col= 'darkgreen' )
    
    control = readline(prompt = "Accept final labeling: press [enter] to continue or [Q] to quit: ")
    while(control != 'q' & control != 'Q' & control != ""){
      print('You must either press [enter] or [Q]')
      control = readline(prompt = "Accept final labeling: press [enter] to continue or [Q] to quit: ")
    }
    
    missed = (control == 'q'| control == 'Q')
    if(missed == TRUE){
      print("Oups! Missed control of final labeling. Skipped and written in log")
      error <- 'Missed control'
      detect <- c(1)
      durations <- c()
      return(list(detect, durations, error))
    }else{
      durations <- sound_detect$s
      return(list(detect, durations, error))
    }
    
  }else{
    print(error)
    detect <- c()
    durations <- c()
    return(list(detect, durations, error))
  }
  
}


if (ok_globals){
  print("Function 'semi_manual_labeling(sound, fs, file_name, threshold, power, min_freq, max_freq, env_window_length, env_window_ov, min_dur, segment_label, accepted_labels)' loaded")
  print(paste0("WL = ", WL))
  print(paste0("MODE: SHOW_STEPS = ", SHOW_STEPS))
}
