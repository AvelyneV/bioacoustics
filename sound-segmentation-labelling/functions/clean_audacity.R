## ------/!\------ ##
## /!\ WARNING /!\
## ------/!\------ ##
##
## Global variables:
## WL: window length for FFT
## WAVE_EXTENSION: string wave files extension
##
## ------------------------
globals <- c('SHOW_STEPS')
ok_globals <- TRUE
for (g in globals) {
  if (!exists(g)) {
    print("======= ALERT =======")
    print(sprintf("The variable '%s' is not defined.", g))
    print("This is not gonna work.")
    print("=====================")
    ok_globals <- FALSE
  }
}

### ---------------------------------
### --- CLEAN AUDACITY FILE FUNCTION 
### ---------------------------------
clean_audacity_file <- function(path_to_rec_folder, file, col1, col2, col3) {
  ## path_to_rec_folder = directory where the data are
  
  ## file = a string with the file name of the source txt file
  
  ## col1, col2, col3 = strings of the names of the columns for the
  ## clean txt file

  ## Action: checks if formatted with the right headers and without 
  ## additional '\\' in rows. If not, cleans it.
  
  ## Output = a table saved as a txt file, with sep = '\t' and
  ## extension '.clean.txt' and the dataframe of the label track

  
  print(paste0("Working on file: ", file))
  
  tab <- read.table(file.path(path_to_rec_folder, file), sep = "\t")
  clean_headers <- tab[1,1] == col1
  
  if(clean_headers == FALSE){
    
    if(SHOW_STEPS){
      print("The first rows of the Audacity file")
      print(tab[1:4,])
      print("No headers + some random crap in some rows")
      readline(prompt = 'Press [enter] to continue')
    }
    
    tab <- tab[tab$V1 != "\\", ]
    names(tab) <- c(col1, col2, col3)
    tab['index'] <- c(1:nrow(tab))
    
    if(SHOW_STEPS){
      print("Now it is looking better with the colum names you chose")
      print(tab[1:4,])
      readline(prompt = 'Press [enter] to continue')
    }
    
    file_wo_ext <- gsub(".txt", "", file)
    new_file_name <- paste(file_wo_ext, '.clean.txt', sep = '' )
    write.table(tab,file.path(path_to_rec_folder, new_file_name), row.names = F, sep ='\t', quote = FALSE)
    print(paste0("Clean audacity file: ", new_file_name, " saved in recording directory" ))
    
  }else{
    
    tab <- read.table(file.path(path_to_rec_folder, file), h = T, sep = "\t")
    tab['index'] <- c(1:nrow(tab))
    file_wo_ext <- gsub(".txt", "", file)
    new_file_name <- paste(file_wo_ext, '.clean.txt', sep = '' )
    write.table(tab,file.path(path_to_rec_folder, new_file_name), row.names = F, sep ='\t', quote = FALSE)
    print(paste0("Clean audacity file: ", new_file_name, " saved in recording directory" ))

  }
  return(tab)
}

### ---------------------------------
### --- CLEAN AUDACITY DIR FUNCTION 
### ---------------------------------
clean_audacity_dir <- function(path_to_rec_folder, col1, col2, col3){
  ## prerequisite = having source txt files with, tab separated columns and 
  ## at least three columns: start, end, label

  ## path_to_rec_folder = directory where the data are

  ## col1, col2, col3 = strings of the names of the columns for the
  ## clean txt file

  ## Action: going through all the files of a directory and run
  ## function clean_audacity_file() on each of them
  
  txt_files_list <- grep(list.files(file.path(path_to_rec_folder), pattern = "*.txt"),
                         pattern = c("*clean"),
                         invert = TRUE, value = TRUE)  
  if(SHOW_STEPS){
    print("List of source txt files that will be checked")
    print(txt_files_list)
    print("The filter is '*.txt' files WITHOUT 'clean' in it.")
  }
  first = TRUE
  for(file in txt_files_list){

    ## very long use of tryCatch for a simple thing: we show steps for at most
    ## one file
    tryCatch(
    {
      show_steps_bu <- SHOW_STEPS
      if (!first) {
        SHOW_STEPS <<- FALSE
        print("Working on all other files at once")
      }
      first <- FALSE

      ## here is the main call
      tab <- clean_audacity_file(path_to_rec_folder, file, col1, col2, col3)
    },
    error = function(cond) {
      message(cond)
      return(NA)
    },
    warning = function(cond) {
      message(cond)
      return(NA)
    },
    finally = {
      SHOW_STEPS <<- show_steps_bu
    }
    )
    
  }
}

if(ok_globals == TRUE){
  print("Functions 'clean_audacity_dir(path_to_rec_folder, col1, col2, col3)' and 'clean_audacity_file(path_to_rec_folder, file, col1, col2, col3)' loaded")
  print(paste0("Mode SHOW_STEPS = ", SHOW_STEPS))
}

