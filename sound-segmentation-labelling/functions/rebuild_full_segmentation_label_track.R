
print("functions loaded: rebuild_full_segmentation_label_track(path_to_data, rec, path_to_output,json_rule) and rebuild_full_segmentation_label_track_dir(path_to_data, path_to_output,json_rule)")

rebuild_full_segmentation_label_track <- function(path_to_data, rec, path_to_output,json_rule){
  
  ## the chunk file with start and end of each chunk
  chunk_file_name <- dir(file.path(path_to_data, rec), '.txt')
  chunk_file <- file.path(path_to_data,rec, chunk_file_name)
  chunks_data <- read.table(chunk_file, h = T, sep ='\t')
  
  ## the autodetect label tracks
  autodetect_dir <- file.path(path_to_data, rec,'output_auto_segmentation_labeling', 'label_tracks')
  autodetect_list <- dir(autodetect_dir, '.txt')
  
  ## initialize a new dataframe for the complete label track
  complete_labels <- data.frame(matrix(ncol = 3, nrow = 0))
  columns <- c('start','end','label')
  colnames(complete_labels) <- columns
  
  for(autodetect in autodetect_list){
    
    autodetect_tab <- read.table(file.path(autodetect_dir,autodetect), h = T, sep = '\t')
    origin <- autodetect_tab$origin[1]
    
    separator <- fromJSON(file=json_rule)$sep
    chunk_id <- unlist(strsplit(origin, separator))[1]
    
    start_chunk <- chunks_data[chunks_data$label == chunk_id,]$start
    
    ## new sync labels
    sync_start <- as.numeric(autodetect_tab$start) + start_chunk
    sync_start <- as.data.frame(sync_start)
    sync_start['sync_end']<- as.numeric(autodetect_tab$end) + start_chunk
    sync_data <- sync_start
    colnames(sync_data) <- c('start', 'end')
    sync_data['label'] <- autodetect_tab$label
    
    complete_labels <- rbind(complete_labels,sync_data)
  }
  
  ## save the track
  full_label_track_name <- paste(chunks_data$origin[1], 'full_segmentation_label_track.txt', sep = '.')
  full_label_track_name <- file.path(path_to_output, full_label_track_name)
  
  write.table(complete_labels,full_label_track_name, sep = '\t', row.names = F, quote = F)
  print(paste0("full segmentation label track for recording '", rec, "' in the raw data folder"))
  return(complete_labels)
}


### on all sessions
rebuild_full_segmentation_label_track_dir <- function(path_to_data, path_to_output,json_rule){
  for(rec in rec_list){
    print(paste0("Working on directory: ", rec))
    complete_labels <- build_complete_label_track(session_pattern)
  }
  
}
