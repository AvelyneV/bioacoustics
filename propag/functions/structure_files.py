import csv
import os
import shutil
import glob
import json
import re
import pandas
from pathlib import Path

WAVE_EXTENSION = "WAV"

def consent(msg):
    answer = input(msg + " [y]/n\n")
    if answer.lower() in ['y', 'yes', '']:
        return True
    else:
        return False


def mkdir_or_quit(folder):
    ok = True
    if not os.path.exists(folder):
        msg = (f"The folder {folder} does not exist. "
               + f"Do you want to create it?")
        if consent(msg):
            os.mkdir(folder)
        else:
            print("I cannot work in these conditions. I quit.")
            ok = False
    return ok


def compute_individual_parameters(aggregated_parameters_filename=None,
                                  raw_parameter_table=None,
                                  headers_line=0):

    if not os.path.isfile(aggregated_parameters_filename):
        with open(aggregated_parameters_filename, 'w') as json_file:
            json_file.write('{".":{}}')

    with open(aggregated_parameters_filename) as json_file:
        agg_parameter_dic = json.load(json_file)

    with open(raw_parameter_table) as p_file:
        tsv_file = csv.reader(p_file, delimiter="\t")
        rec_files = list(tsv_file)

        # extract headers
        headers = []
        if headers_line is not None:
            headers = rec_files[headers_line]
            rec_files = rec_files[headers_line+1:]

        # we need an entry with the original file name
        if "origin" not in headers:
            raise Exception(f'We need to have an "origin" header, that corresponds'
                            f' to the original name of the file')
        iorigin = headers.index("origin")

        # No problem with the headers?
        headers_set = set(headers)
        if len(headers_set) != len(headers):
            raise Exception(f"There are similar headers in the table file... "
                            f"I read: {headers}")

        # extract and clean date, from org-mode export format to YYYY-MM-DD
        if "date" in headers:
            idate = headers.index("date")
            for f in rec_files:
                f[idate] = f[idate].strip('<>').split(' ')[0]

        # filter if parameter table contains too much info
        iparam_to_filter = []
        iparam_to_include = []
        if '.' in agg_parameter_dic:
            common_parameters = agg_parameter_dic['.']
            for ih in range(len(headers)):
                if headers[ih] in agg_parameter_dic['.']:
                    iparam_to_filter.append(ih)
                else:
                    iparam_to_include.append(ih)

        if iorigin in iparam_to_filter:
            raise Exception(f"You are not supposed to have 'origin' as a "
                            f"common parameter.")
        iparam_to_include.remove(iorigin)

        spec_parameters = {}

        for f in rec_files:
            # filter out measures that do not fit with the common parameters
            filtered_out = False
            for ih in iparam_to_filter:
                header = headers[ih]
                if common_parameters[header] != f[ih]:
                    filtered_out = True
                    break

            if not filtered_out:
                if str(f[iorigin]) in spec_parameters:
                    raise Exception(f"Several files with same origin, "
                                    f"e.g. {f[iorigin]}")
                spec_parameters[str(f[iorigin])] = (
                    {headers[ih]: f[ih] for ih in iparam_to_include})

    return agg_parameter_dic, spec_parameters


def dump_parameters_in_raw(src_path,
                           raw_parameter_table=None,
                           aggregated_parameters_filename=None,
                           keep_previous_indiv_parameters=False,
                           headers_line=1
                           ):
    '''
    Parameters
    ----------
    src_path: string
              raw data directory
    raw_parameter_table: string
                         absolute path to the initial parameter table
    aggregated_parameters_filename: string
                                    absolute path to the parameter json file
    keep_previous_indiv_parameters: boolean
                                    do we keep previous individual parameters?
    Summary
    -------
    - Transform the tsv file into a more friendly json file
    - The keys are the "roots" of the files and the values are dictionaries
      of {parameter -> value}
    - Note that the top dictionary may contain the key '.' whose value is
      the shared parameters (with their values)
    '''

    init_dic, add_dic = compute_individual_parameters(
        aggregated_parameters_filename,
        raw_parameter_table,
        headers_line=headers_line)

    if not keep_previous_indiv_parameters:
        key_to_remove = list(init_dic.keys())
        if '.' in key_to_remove:
            key_to_remove.remove('.')
        if '..' in key_to_remove:
            key_to_remove.remove('..')
        for k in key_to_remove:
            del init_dic[k]

    for k in add_dic.keys():
        if k in init_dic:
            init_dic[k] = {**init_dic[k], **add_dic[k]}
        else:
            init_dic[k] = add_dic[k]

    with open(os.path.join(src_path, "parameters.json"), 'w') as outfile:
        json.dump(init_dic, outfile, indent=2)


def compute_chunk_parameters(sync_parameter_table):
    all_chunks = []
    with open(sync_parameter_table) as p_file:
        chunk_reader = csv.DictReader(p_file, delimiter="\t")
        for chunk in chunk_reader:
            all_chunks.append({k:v for k,v in chunk.items()
                               if k not in ["startsync", "endsync"]})
    return all_chunks


def dump_chunk_parameters_in_raw_dispatch(src_path,
                                          parameters_base_filename,
                                          rule_json_file,
                                          chunk_synchro_file_extension):
    parameters_file = os.path.join(src_path, "parameters.json")
    with open(parameters_file) as json_file:
        parameters = json.load(json_file)
    src_parameters = dict()
    if '.' in parameters:
        src_parameters = parameters['.']
        del parameters['.']


    with open(rule_json_file) as json_file:
        rule = json.load(json_file)
        expression = rule["chunk"]

    for record, common_param in parameters.items():
        record_parameters = dict()
        record_parameters['.'] = {**src_parameters, **common_param}
        record_parameters['.']['origin'] = record
        chunk_synchro_file = os.path.join(src_path,
                                          record + chunk_synchro_file_extension)
        if not os.path.exists(chunk_synchro_file):
            print("WARNING")
            print(f"Did not find {chunk_synchro_file}")
            print("Ignored.")
            continue
        chunks_parameters = compute_chunk_parameters(chunk_synchro_file)
        for params in chunks_parameters:
            missing, chunk_filename, full_context = replace_fstring(expression,
                                                                    params)
            if missing:
                print(f"File {chunk_filename}:\n"
                      f"The rule needs parameters: '{', '.join(missing)}'; "
                      f"couldn't find it")
            # more checks needed
            if chunk_filename in record_parameters:
                raise Exception("chunk already found")
            record_parameters[chunk_filename] = params

        with open(os.path.join(src_path, record, "parameters.json"), 'w') \
             as outfile:
            json.dump(record_parameters, outfile, indent=2)

def rule_file_look_up(src_path,
                      dst_path,
                      rule_json_file=None):
    '''
    Look for the rule json file.
    '''
    if rule_json_file is None:
        rule_json_file = os.path.join(src_path, "rule.json")
    if not os.path.exists(rule_json_file):
        src_rule = os.path.join(src_path, rule_json_file)
        if os.path.exists(src_rule):
            rule_json_file = src_rule
        else:
            dst_rule = os.path.join(dst_path, rule_json_file)
            if os.path.exists(dst_rule):
                rule_json_file = dst_rule
            else:
                raise Exception(
                    f"Couldn't find any rule file. Tried {rule_json_file}, "
                    f"{src_rule} and {dst_rule}.")
    return rule_json_file


def replace_fstring(expression, context, shared_context=dict()):
    missing_variables = []
    pattern = re.compile(r'\{\w*\}')

    filled_expression = ""
    icurrent = 0
    full_context = {k:v for k,v in context.items()}
    for match in pattern.finditer(expression):
        s, e = match.span()
        between = expression[icurrent:s]
        variable = expression[s+1:e-1]
        value = ""
        if variable not in context:
            if variable not in shared_context:
                value = ""
                missing_variables.append(variable)
            else:
                value = shared_context[variable]
                full_context[variable] = value
        else:
            value = context[variable]
        filled_expression += between + value
        icurrent = e

    return missing_variables, filled_expression, full_context


def generate_link(src_path,
                  dst_path,
                  rule_json_file=None,
                  parameters_json_file=None,
                  symbolic=True):
    '''
    Parameters
    ----------
    src_path: string
              raw data directory
    dst_path: string
              directory where you will have the (fake) files with fully
              qualified names
    rule_json_file: string
                    absolute path to the file that rules the name generation
                    (file formatted as a python f-string)
    parameters_json_file: string
                          absolute path to the parameter json file
    symbolic: boolean
              links are symbolic (versus file copies)
    Summary
    -------
    Based on the rule json file and on the parameters associated with each file,
    create symbolic links in dst_path that will point to the right files in
    src_path.
    Generates another parameter json file in the dst_path that maps reversely
    the new names onto the corresponding parameters.
    '''
    if parameters_json_file is None:
        parameters_json_file = os.path.join(src_path, "parameters.json")

    with open(rule_json_file) as json_file:
        rule = json.load(json_file)
        expression = rule["prefix"] + rule["separator"] + rule["recording"]

    with open(parameters_json_file) as json_file:
        parameters = json.load(json_file)

    if '.' in parameters:
        shared_context = parameters['.']
        del parameters['.']
    else:
        shared_context = dict()
    if '..' in parameters:
        del parameters['..']

    linkto = {}
    reverselinkto = {}

    for file, context in parameters.items():
        context['origin'] = file

        missing, new_filename, full_context = replace_fstring(expression,
                                                              context,
                                                              shared_context)
        if missing:
            print(f"File {file}:\n"
                  f"The rule needs parameters: '{', '.join(missing)}'; "
                  f"couldn't find it")
        full_context['linkto'] = file
        if new_filename in linkto:
            raise Exception(f"With the given rule ({expression}), two files ("
                            f"{linkto[new_filename]} and {file}) will map to "
                            f"the same file ({new_filename})")
        linkto[new_filename] = full_context
        reverselinkto[file] = new_filename

    dst_parameters_path = os.path.join(dst_path, "parameters.json")
    previous_parameters = {}
    if os.path.exists(dst_parameters_path):
        with open(dst_parameters_path) as json_file:
            previous_parameters = json.load(json_file)

    for f in linkto:
        if f in previous_parameters:
            raise Exception(f"The prefix {f} already exists.")

    previous_parameters = {**previous_parameters, **linkto}

    with open(dst_parameters_path, 'w') as outfile:
        json.dump(previous_parameters, outfile, indent=2)

    for file in parameters:
        full_file = os.path.join(src_path, f"{file}*")
        extended_files = [os.path.basename(f) for f in glob.glob(full_file)]
        extensions = [s[len(file):] for s in extended_files]
        for e in extensions:
            source = os.path.join(src_path, f"{file}{e}")
            if not symbolic:
                if os.path.isdir(source):
                    link = shutil.copytree
                else:
                    link = shutil.copyfile
            else:
                link = os.symlink
            link(source,
                 os.path.join(dst_path, f"{reverselinkto[file]}{e}"))


def flatten_chunks(src_path,
                   dst_path,
                   rule_json_file,
                   ignore_list=[""],
                   symbolic=True):
    '''
    Parameters
    ----------
    src_path: string
              directory containing sounds (might be symbolic link) with fully
              qualified names, directories containing the chunks and a parameter
              json file
    dst_path: string
              directory where the chunk sounds will all be gathered (with
              their fully qualified names)
    ignore_list: list of string
                 list of directories that should not be considered as directories
                 that contain chunks of sound
    symbolic: boolean
              links are symbolic (versus file copies)
    Summary
    -------
    Copies all the chunks contained in sub-directory of src_path into dst_path.
    The name of a chunk will be the fully qualified name of the total recording
    followed by the chunk name (separated by "~").
    Generates also a parameter json file in dst_path.
    '''
    with open(rule_json_file) as json_file:
        rule = json.load(json_file)
        rec_exp = rule["prefix"] + rule["separator"] + rule["recording"]
        chunk_exp = rule["chunk"]
        full_exp = rec_exp + rule["separator"] + chunk_exp

    with open(os.path.join(src_path, "parameters.json")) as outfile:
        full_parameters = json.load(outfile)

    # TODO should not be ignored
    if '.' in full_parameters:
        print("Strange that you have common parameters in {src_path}. Ignored")
        del full_parameters['.']

    # should rather ask if ignore or not
    chunk_directories = []
    for d in os.listdir(src_path):
        true_path = Path(os.path.join(src_path, d)).resolve()
        if os.path.isdir(true_path) and d not in ignore_list:
            chunk_directories.append(d)

    flat_parameters = dict()
    for recording in full_parameters:
        if recording in ignore_list:
            print("{chunk} ignored (as required).")
            continue

        rec_param_file = os.path.join(src_path, recording, "parameters.json")

        if not os.path.exists(rec_param_file):
            print("WARNING")
            print(f"{recording} has no parameters.json file")
            print("Ignored.")
            continue
        # get parameters for the chunks of the recording
        with open(rec_param_file) as outfile:
            rec_parameters = json.load(outfile)
        rec_shared_parameters = dict()
        if '.' in rec_parameters:
            rec_shared_parameters = rec_parameters['.']
            del rec_parameters['.']

        # should be for wave files
        for chunk_name, chunk_parameters in rec_parameters.items():
            missing, chunk_full_name, _ = \
                replace_fstring(full_exp,
                                chunk_parameters,
                                shared_context=rec_shared_parameters)
            if missing:
                print(f"File {chunk_name} in {recording}:\n"
                      f"The rule needs parameters: '{', '.join(missing)}'; "
                      f"couldn't find it")

            if symbolic:
                link = os.symlink
            else:
                link = shutil.copyfile
            link(os.path.join(src_path, recording,
                              chunk_name + "." + WAVE_EXTENSION),
                 os.path.join(dst_path,
                              chunk_full_name + "." + WAVE_EXTENSION))
            flat_parameters[chunk_full_name] = {**rec_shared_parameters,
                                                **chunk_parameters}


    with open(os.path.join(dst_path, "parameters.json"), 'w') as outfile:
        json.dump(flat_parameters, outfile, indent=2)


def merge_final_parameters(parameter_dir):
    '''
    Combine all parameters in os.path.join(parameter_dir, "parameters.json")
    into a new os.path.join(parameter_dir, "parameters.csv")
    '''
    with open(os.path.join(parameter_dir, "parameters.json")) as json_file:
        parameter_dic = json.load(json_file)

    json_to_csv = [{**{'filename': k}, **v} for k,v in parameter_dic.items()]
    dataframe = pandas.read_json(json.dumps(json_to_csv))
    dataframe.to_csv(os.path.join(parameter_dir, "parameters.csv"), index=False)


def generate_all(
        src_paths,
        dst_path,
        rule_json_file,
        chunk_synchro_file_extension=".sync.txt",
        sym_full=True,
        sym_chunks=True
    ):
    '''
    Parameters
    ----------
    src_paths: list of string
               every element is the name of a folder containing
               - recording files (full sounds)
               - one directory per recording file containing its chunks
               - a "parameters.json" file
    dst_path: string
              directory where the sounds will all be gathered (with
              their fully qualified names)
    rule_json_file: string
                    absolute path to a json file containing a "rule" key that
                    defines a rule to generate the symbolic links
    chunk_synchro_file_extension: string
                                  extension for synchronization files
    sym_full: boolean
              generates symbolic links for the structured db (versus copies
              directories)
    sym_chunks: boolean
                generates symbolic links for the flat db (versus copies files)

    Summary
    -------
    - In every directory of src_paths
      - creates a folder whose name is the basename of the origin recording file
        and creates inside one WAV file per chunk
      - completes the parameters.json file relying on the parameters.tsv file
    - In dst_path/full: creates symbolic links (according to rule.json) that
      points to the recording files
    - In dst_path/chunks:
      - creates symbolic links (according to rule.json) that points to the
        chunks of the recording files
      - creates a parameters.json and a parameters.csv containg the meta-data
        of the raw data
    '''
    for src_path in src_paths:
        if not mkdir_or_quit(src_path):
            return

    if not mkdir_or_quit(dst_path):
        return

    dst_path_structured = os.path.join(dst_path, "full")
    dst_path_flattened = os.path.join(dst_path, "chunks")
    if sym_full:
        dst_path_structured += "_symlink"
    else:
        dst_path_structured += "_wav"
    if sym_chunks:
        dst_path_flattened += "_symlink"
    else:
        dst_path_flattened += "_wav"

    if os.path.exists(dst_path_structured):
        shutil.rmtree(dst_path_structured)
    os.mkdir(dst_path_structured)
    if os.path.exists(dst_path_flattened):
        shutil.rmtree(dst_path_flattened)
    os.mkdir(dst_path_flattened)

    parameters_json_base_filename = "parameters.json"

    ignore_list = []
    headers_line = 0

    for src_path in src_paths:
        raw_parameter_table = os.path.join(src_path, "parameters.tsv")
        aggregated_parameters_filename = os.path.join(src_path,
                                                      parameters_json_base_filename)
        if rule_json_file is None:
            rule_json_file = rule_file_look_up(src_path, dst_path_structured, None)
        dump_parameters_in_raw(
            src_path,
            raw_parameter_table,
            headers_line=headers_line,
            aggregated_parameters_filename=aggregated_parameters_filename,
            keep_previous_indiv_parameters=False
        )
        dump_chunk_parameters_in_raw_dispatch(
            src_path,
            parameters_json_base_filename,
            rule_json_file,
            chunk_synchro_file_extension
        )
        generate_link(src_path,
                      dst_path_structured,
                      rule_json_file=rule_json_file,
                      parameters_json_file=aggregated_parameters_filename,
                      symbolic=sym_full)

    flatten_chunks(dst_path_structured,
                   dst_path_flattened,
                   rule_json_file,
                   ignore_list=ignore_list,
                   symbolic=sym_chunks)
    merge_final_parameters(dst_path_structured)
    merge_final_parameters(dst_path_flattened)
