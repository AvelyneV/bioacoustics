# Propagation experiment
*cf.* [here](./propag)

# Sound segmentation and labeling workshop
*cf.* [here](./sound-segmentation-labelling)

Last update date 2023-03-29:
- `dispatch_sound` function was split but the behaviour remained the same

# Vocalisation structure analysis
*cf.* [here](./vocalisation-structure)
